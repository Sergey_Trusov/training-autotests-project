package behavior.pages;

import behavior.steps.BaseSteps;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Элементы для сайта tele2
 */
public class Tele2Page extends BaseSteps {

    /**
     * url сайта
     */
    public String siteUrl = "https://msk.tele2.ru";


    /**
     * Коллекция кнопок "Настроить тариф"
     */
    public ElementsCollection tariffSetUpButtonColection = $$(byXpath("//div[contains(@class,'settings-link')]"));

    /**
     * Заголовок цены для тарифа
     */
    public SelenideElement priceForTarriff = $(byXpath("//div[contains(@class,'hidden-xs')]//span[contains(@class,'price')]"));

    /**
     * Коллекция услуг для услуг тарифа
     */
    public ElementsCollection servicesCollection = $$(byXpath("//div[contains(@class,'iconed-services-group')]//div[contains(@class,'item')]/img"));

<<<<<<< HEAD
    /**
     * url сайта
     */
    public String siteUr2 = "https://mail.ru";

    /**
     * 1 инпут ввода почты
     */
    public SelenideElement enterEmailString = $(By.xpath("//div//input[contains(@class,'input i-no-right-radius i-width-100% mailbox__rwd-control')]"));

    /**
     * 2  ввод учетной записи
     */
    public static SelenideElement entersAccount = $(By.xpath("//div//input[contains(@class,'input i-no-right-radius i-width-100% mailbox__rwd-control')]"));

    /**
     * 3 кнопка входа
=======
    // почта


    public String site = " https://mail.ru";
    /**
     * 1 строка имя ящика
     */
    public SelenideElement userClicksOnTheStringBoxName = $(By.xpath("//div//input[contains(@class,'input i-no-right-radius i-width-100% mailbox__rwd-control')]"));

    /**
     * 2  вводит учетную запись
     */
    public static SelenideElement userEntersAccount = $(By.xpath("//div//input[contains(@class,'input i-no-right-radius i-width-100% mailbox__rwd-control')]"));

    /**
     * 3 клик ввести пароль
>>>>>>> 7eb3ec487c41cff217cddcec99043a2cf9058a81
     */
    public SelenideElement userClicks = $(By.xpath("//div//label[contains(@id,'mailbox:submit')]"));

    /**
     * 4 клик на пароль
     */

<<<<<<< HEAD
    public SelenideElement enterPassword = $(By.xpath("//div//input[contains(@class,'input mailbox__input mailbox__input_password mailbox__rwd-control')]"));

    /**
     * инпут ввода пороля
=======
    public SelenideElement userClicksOnLineEnterPassword = $(By.xpath("//div//input[contains(@class,'input mailbox__input mailbox__input_password mailbox__rwd-control')]"));

    /**
     * ввод пароля
>>>>>>> 7eb3ec487c41cff217cddcec99043a2cf9058a81
     */
    public SelenideElement passwordInput = $(By.xpath("//div//input[contains(@class,'input mailbox__input mailbox__input_password mailbox__rwd-control')]"));

    /**
<<<<<<< HEAD
     * 6   кнопка войти
=======
     * 6  войти
>>>>>>> 7eb3ec487c41cff217cddcec99043a2cf9058a81
     */
    public SelenideElement entrance = $(By.xpath("//div//label[contains(@id,'mailbox:submit')]"));


    /**
<<<<<<< HEAD
     * url сайта
     */
    public String siteUr3 = "https://moscow.megafon.ru/";

    /**
     * переход в Устройства
     */
    public SelenideElement deviceButton = $(By.xpath("//div[contains(@class,'b-tile-v2 b-tile-v2_height_small b-tile-v2_default_100 i-bem b-tile-v2_js_inited')]"));

    /**
     * Переход в смартфоны
     */
    public ElementsCollection smartphoneButton = $$(By.xpath("//span[contains(text(),\"Смартфоны\")]"));

    /**
     * Раздел сортировки
     */
    public SelenideElement sortButton = $(By.xpath("//div[contains(@class,'b-sort-panel__active')]"));

    /**
     * верху дороже
     */
    public SelenideElement topExpensive = $(By.xpath("//div//span[contains(@class,'b-sort-panel__link sort price desc')]"));

    /**
     * Клик на 1 элемент
     */
    public ElementsCollection firstItem = $$(By.xpath("//div[contains(@class,'b-good__inside good')]"));

    /**
     * Сравнение цены 131 990
     */
    public SelenideElement priceShouldBeText = $(By.xpath("//div//span[contains(@class,'b-price-cards__actual   b-price__actual')]"));

    /**
     * 64гб
     */
    public ElementsCollection gig = $$(By.xpath("//div//a[contains(@class,'variantText')]"));

    /**
     * Сравнение цены 99 990
     */
    public SelenideElement Prices = $(By.xpath("//div//span[contains(@class,'b-price-cards__value  b-price__value')]"));



    /**
     * Кнопка мобильная связь
     */
    public ElementsCollection buttonMobileCommunications = $$(By.xpath("//div//span[contains(@class,'section-title')]"));

    /**
     * Кнопка выбора номера
     */
    public ElementsCollection selectNumber = $$(By.xpath("//div//a[contains(@class,'old-menu-link')]"));

    /**
     * Инпут поиска
     */
    public SelenideElement numberSearch = $(By.xpath("//div//input[contains(@class,'text-field')]"));

    /**
     * Ввод номера
     */
    public SelenideElement number = $(By.xpath("//div//input[contains(@class,'text-field')]"));

    /**
     * Выбор из списка
     */
    public SelenideElement numberFromTheList = $(By.xpath("//div[contains(@class,'phone-number-block')]"));

    /**
     * Сравнение номера
     */
    public SelenideElement theNumberIsDisplayed = $(By.xpath("//div//span[contains(@class,'selected-phone')]"));


=======
     * Мегафон покупка
     */
    public SelenideElement theTransitionInTheDevice = $(By.xpath("//div[contains(@class,'b-tile-v2 b-tile-v2_height_small b-tile-v2_default_100 i-bem b-tile-v2_js_inited')]"));
    public ElementsCollection switchingToASmartphone = $$(By.xpath("//span[contains(text(),\"Смартфоны\")]"));
    public SelenideElement userClicksOnSorting = $(By.xpath("//div[contains(@class,'b-sort-panel__active')]"));
    public SelenideElement sort = $(By.xpath("//div//span[contains(@class,'b-sort-panel__link sort price desc')]"));
    public ElementsCollection thirdElement = $$(By.xpath("//div[contains(@class,'b-good__inside good')]"));
    public SelenideElement   priceShouldBeText = $(By.xpath("//div//span[contains(@class,'b-price-cards__actual   b-price__actual')]"));
    public ElementsCollection gig =$$(By.xpath("//div//a[contains(@class,' variantText')]"));
    public SelenideElement Prices =$(By.xpath("//div//span[contains(@class,'b-price-cards__value  b-price__value')]"));
>>>>>>> 7eb3ec487c41cff217cddcec99043a2cf9058a81
}
