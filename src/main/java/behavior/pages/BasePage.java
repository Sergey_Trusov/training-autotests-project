package behavior.pages;

import behavior.TestRunner;
import com.codeborne.selenide.SelenideElement;
import org.junit.Assert;
import org.openqa.selenium.Alert;

import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.Assert.assertEquals;

public class BasePage extends TestRunner {
    public Alert userEntersAccount;

    /**
     * Открывает переданный сайт
     *
     * @param site
     */
    public static void openSite(String site) {
        System.setProperty("webdriver.chrome.driver", "src/main/drivers/chromedriver.exe");
        open(site);
    }

    /**
     * Кликает на элемент по xpath
     *
     * @param element
     */
    public static void clickOnElement(SelenideElement element) throws InterruptedException {
        Thread.sleep(3000);
        element.click();
    }

    /**
     * Кликает на элемент по xpath
     *
     * @param elements
     * @param number
     */
    public static void clickOnElementInCollection(List<SelenideElement> elements, String number) throws InterruptedException {
        Thread.sleep(3000);
        elements.get(Integer.parseInt(number)).click();
    }

    /**
     * Проверяет текст элемента с актуальным
     *
     * @param element
     * @param text
     */
    public static void elementShouldBeText(SelenideElement element, String text) throws InterruptedException {
        Thread.sleep(3000);
        assertEquals(element.getText().replace(" ",""),text);
    }

    /**
     * Проверяет текст элемента с актуальным
     *
     * @param actual
     * @param expected
     */
    public static void slugShouldBeText(String actual, String expected) throws InterruptedException {
        Thread.sleep(3000);
        assertEquals(actual, expected);
    }

    }
