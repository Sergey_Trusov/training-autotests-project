package behavior;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/features/Test.feature",
        glue = "behavior/steps",
<<<<<<< HEAD
        tags = "@tel",
=======
        tags = "@meg",
>>>>>>> 7eb3ec487c41cff217cddcec99043a2cf9058a81
        dryRun = false,
        strict = true,
        snippets = SnippetType.UNDERSCORE
)
public class TestRunner {
    public static WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        setWebDriver(driver);
    }

    @After
    public void close() {
        getWebDriver().quit();
    }
}

